
function fibonacci(F0, F1, n) {
    if (n < 0) {
        return fibonacci(n * -1 - 1, n * -1 - 2, n * -1) * -1;
    };

    if ((n == 0) || (n == 1)) {
        return n;
    } else {
        return fibonacci(F0 - 1, F0 - 2, F0) + fibonacci(F1 - 1, F1 - 2, F1);
    };
};

const nTerms = prompt('Enter the number of terms: ');

alert(fibonacci(nTerms - 1, nTerms - 2, nTerms));